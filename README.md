This repository contains a fork from http://www.librevisa.org with the intent to port it from Linux to Windows.

The original project implements the VISA API (see http://www.ivifoundation.org/specifications/default.aspx) as an Open Source shared Linux library (libvisa.so) in contrast to Windows libraries which are available as Closed Source from Tektronix or National Instrument and require registration for download.

For the make process, the original uses automake tools, but I have added a layer on top of it using the C++ NetBeans plugin. As the tool chain I am using MSYS2 and MinGW64.

Contact: gfuchs@acousticmicroscopy.com
